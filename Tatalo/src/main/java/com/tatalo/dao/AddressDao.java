/**
 * 
 */
package com.tatalo.dao;

import java.util.ArrayList;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tatalo.entities.Address;
import com.tatalo.entities.User;

/**
 * @author Michael_IT
 *
 */
@Repository
public class AddressDao extends Dao<Address>{
	@Autowired
	SessionFactory sessionFactory;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tatalo.dao.Dao#getAll()
	 */
	@Override
	public ArrayList<Address> getAll() {
		Session session = sessionFactory.getCurrentSession();
		return (ArrayList<Address>) session.createQuery("from Address").list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tatalo.dao.Dao#getById(java.lang.Object)
	 */
	@Override
	public Address getById(Address t) {
		Session session = sessionFactory.getCurrentSession();
		return (Address) session.get(Address.class, t.getId());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tatalo.dao.Dao#add(java.lang.Object)
	 */
	@Override
	public Address add(Address t) {
		Session session = sessionFactory.getCurrentSession();
		session.persist(t);
		return t;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tatalo.dao.Dao#update(java.lang.Object)
	 */
	@Override
	public boolean update(Address t) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.update(t);
			return Boolean.TRUE;
		} catch (Exception e) {
			return Boolean.FALSE;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tatalo.dao.Dao#delete(java.lang.Object)
	 */
	@Override
	public boolean delete(Address t) {
		Session session = this.sessionFactory.getCurrentSession();
		if (null != t) {
			try {
				session.delete(t);
				return Boolean.TRUE;
			} catch (Exception e) {
				return Boolean.FALSE;
			}
		}
		return Boolean.FALSE;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tatalo.dao.Dao#deleteById(java.lang.Object)
	 */
	@Override
	public boolean deleteById(Address t) {
		Session session = this.sessionFactory.getCurrentSession();
		Address address = (Address) session.load(Address.class, t.getId());
		if (null != address) {
			session.delete(address);
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
}
