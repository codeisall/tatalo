/**
 * 
 */
package com.tatalo.dao;

import java.util.ArrayList;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tatalo.entities.Cart;
import com.tatalo.entities.Cart;

/**
 * @author Michael_IT
 *
 */
@Repository
public class CartDao extends Dao<Cart> {
	@Autowired
	SessionFactory sessionFactory;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tatalo.dao.Dao#getAll()
	 */
	@Override
	public ArrayList<Cart> getAll() {
		Session session = sessionFactory.getCurrentSession();
		return (ArrayList<Cart>) session.createQuery("from Cart").list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tatalo.dao.Dao#getById(java.lang.Object)
	 */
	@Override
	public Cart getById(Cart t) {
		Session session = sessionFactory.getCurrentSession();
		return (Cart) session.get(Cart.class, t.getId());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tatalo.dao.Dao#add(java.lang.Object)
	 */
	@Override
	public Cart add(Cart t) {
		Session session = sessionFactory.getCurrentSession();
		session.persist(t);
		return t;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tatalo.dao.Dao#update(java.lang.Object)
	 */
	@Override
	public boolean update(Cart t) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.update(t);
			return Boolean.TRUE;
		} catch (Exception e) {
			return Boolean.FALSE;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tatalo.dao.Dao#delete(java.lang.Object)
	 */
	@Override
	public boolean delete(Cart t) {
		Session session = this.sessionFactory.getCurrentSession();
		if (null != t) {
			try {
				session.delete(t);
				return Boolean.TRUE;
			} catch (Exception e) {
				return Boolean.FALSE;
			}
		}
		return Boolean.FALSE;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tatalo.dao.Dao#deleteById(java.lang.Object)
	 */
	@Override
	public boolean deleteById(Cart t) {
		Session session = this.sessionFactory.getCurrentSession();
		Cart cart = (Cart) session.load(Cart.class, t.getId());
		if (null != cart) {
			session.delete(cart);
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
}
