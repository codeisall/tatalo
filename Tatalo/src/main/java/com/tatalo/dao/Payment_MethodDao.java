/**
 * 
 */
package com.tatalo.dao;

import java.util.ArrayList;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tatalo.entities.Payment_Method;
import com.tatalo.entities.Payment_Method;

/**
 * @author Michael_IT
 *
 */
@Repository
public class Payment_MethodDao extends Dao<Payment_Method> {
	@Autowired
	SessionFactory sessionFactory;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tatalo.dao.Dao#getAll()
	 */
	@Override
	public ArrayList<Payment_Method> getAll() {
		Session session = sessionFactory.getCurrentSession();
		return (ArrayList<Payment_Method>) session.createQuery("from Payment_Method").list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tatalo.dao.Dao#getById(java.lang.Object)
	 */
	@Override
	public Payment_Method getById(Payment_Method t) {
		Session session = sessionFactory.getCurrentSession();
		return (Payment_Method) session.get(Payment_Method.class, t.getId());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tatalo.dao.Dao#add(java.lang.Object)
	 */
	@Override
	public Payment_Method add(Payment_Method t) {
		Session session = sessionFactory.getCurrentSession();
		session.persist(t);
		return t;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tatalo.dao.Dao#update(java.lang.Object)
	 */
	@Override
	public boolean update(Payment_Method t) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.update(t);
			return Boolean.TRUE;
		} catch (Exception e) {
			return Boolean.FALSE;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tatalo.dao.Dao#delete(java.lang.Object)
	 */
	@Override
	public boolean delete(Payment_Method t) {
		Session session = this.sessionFactory.getCurrentSession();
		if (null != t) {
			try {
				session.delete(t);
				return Boolean.TRUE;
			} catch (Exception e) {
				return Boolean.FALSE;
			}
		}
		return Boolean.FALSE;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tatalo.dao.Dao#deleteById(java.lang.Object)
	 */
	@Override
	public boolean deleteById(Payment_Method t) {
		Session session = this.sessionFactory.getCurrentSession();
		Payment_Method payment_method = (Payment_Method) session.load(Payment_Method.class, t.getId());
		if (null != payment_method) {
			session.delete(payment_method);
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
}
