/**
 * 
 */
package com.tatalo.dao;

import java.util.ArrayList;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tatalo.entities.QAndA;
import com.tatalo.entities.QAndA;

/**
 * @author Michael_IT
 *
 */
@Repository
public class QAndADao extends Dao<QAndA> {
	@Autowired
	SessionFactory sessionFactory;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tatalo.dao.Dao#getAll()
	 */
	@Override
	public ArrayList<QAndA> getAll() {
		Session session = sessionFactory.getCurrentSession();
		return (ArrayList<QAndA>) session.createQuery("from QAndA").list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tatalo.dao.Dao#getById(java.lang.Object)
	 */
	@Override
	public QAndA getById(QAndA t) {
		Session session = sessionFactory.getCurrentSession();
		return (QAndA) session.get(QAndA.class, t.getId());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tatalo.dao.Dao#add(java.lang.Object)
	 */
	@Override
	public QAndA add(QAndA t) {
		Session session = sessionFactory.getCurrentSession();
		session.persist(t);
		return t;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tatalo.dao.Dao#update(java.lang.Object)
	 */
	@Override
	public boolean update(QAndA t) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.update(t);
			return Boolean.TRUE;
		} catch (Exception e) {
			return Boolean.FALSE;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tatalo.dao.Dao#delete(java.lang.Object)
	 */
	@Override
	public boolean delete(QAndA t) {
		Session session = this.sessionFactory.getCurrentSession();
		if (null != t) {
			try {
				session.delete(t);
				return Boolean.TRUE;
			} catch (Exception e) {
				return Boolean.FALSE;
			}
		}
		return Boolean.FALSE;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tatalo.dao.Dao#deleteById(java.lang.Object)
	 */
	@Override
	public boolean deleteById(QAndA t) {
		Session session = this.sessionFactory.getCurrentSession();
		QAndA qanda = (QAndA) session.load(QAndA.class, t.getId());
		if (null != qanda) {
			session.delete(qanda);
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
}
