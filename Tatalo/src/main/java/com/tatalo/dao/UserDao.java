/**
 * 
 */
package com.tatalo.dao;

import java.util.ArrayList;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tatalo.entities.User;

/**
 * @author Michael_IT
 *
 */
@Repository
public class UserDao extends Dao<User> {
	@Autowired
	SessionFactory sessionFactory;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tatalo.dao.Dao#getAll()
	 */
	@Override
	public ArrayList<User> getAll() {
		Session session = sessionFactory.getCurrentSession();
		return (ArrayList<User>) session.createQuery("from User").list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tatalo.dao.Dao#getById(java.lang.Object)
	 */
	@Override
	public User getById(User t) {
		Session session = sessionFactory.getCurrentSession();
		return (User) session.get(User.class, t.getId());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tatalo.dao.Dao#add(java.lang.Object)
	 */
	@Override
	public User add(User t) {
		Session session = sessionFactory.getCurrentSession();
		session.persist(t);
		return t;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tatalo.dao.Dao#update(java.lang.Object)
	 */
	@Override
	public boolean update(User t) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.update(t);
			return Boolean.TRUE;
		} catch (Exception e) {
			return Boolean.FALSE;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tatalo.dao.Dao#delete(java.lang.Object)
	 */
	@Override
	public boolean delete(User t) {
		Session session = this.sessionFactory.getCurrentSession();
		if (null != t) {
			try {
				session.delete(t);
				return Boolean.TRUE;
			} catch (Exception e) {
				return Boolean.FALSE;
			}
		}
		return Boolean.FALSE;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tatalo.dao.Dao#deleteById(java.lang.Object)
	 */
	@Override
	public boolean deleteById(User t) {
		Session session = this.sessionFactory.getCurrentSession();
		User user = (User) session.load(User.class, t.getId());
		if (null != user) {
			session.delete(user);
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
}
