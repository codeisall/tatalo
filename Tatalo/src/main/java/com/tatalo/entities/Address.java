/**
 * 
 */
package com.tatalo.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * @author Michael_IT
 *
 */
@Entity
@Table(name = "address", catalog="tatalo")
public class Address {
	private int id;
	private String detail;
	private City city_id;
	private District district_id;

	public Address(int id, String detail, City city_id, District district_id) {
		super();
		this.id = id;
		this.detail = detail;
		this.city_id = city_id;
		this.district_id = district_id;
	}

	public Address() {
		super();
	}

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name="detail")
	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="city_id", referencedColumnName="id")
	public City getCity_id() {
		return city_id;
	}

	public void setCity_id(City city_id) {
		this.city_id = city_id;
	}

	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="district_id", referencedColumnName="id")
	public District getDistrict_id() {
		return district_id;
	}

	public void setDistrict_id(District district_id) {
		this.district_id = district_id;
	}
}
