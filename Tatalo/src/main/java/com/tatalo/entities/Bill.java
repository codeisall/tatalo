/**
 * 
 */
package com.tatalo.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * @author Michael_IT
 *
 */
@Entity
@Table(name="bill", catalog="tatalo")
public class Bill {
	private int id;
	private User user_id;
	private String order_date;
	private String paid_date;
	private String delivery;
	private Payment_Method payment_method;
	private String status;

	public Bill(int id, User user_id, String order_date, String paid_date, String delivery,
			Payment_Method payment_method, String status) {
		super();
		this.id = id;
		this.user_id = user_id;
		this.order_date = order_date;
		this.paid_date = paid_date;
		this.delivery = delivery;
		this.payment_method = payment_method;
		this.status = status;
	}

	public Bill() {
		super();
	}

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name="user_id", referencedColumnName="id")
	public User getUser_id() {
		return user_id;
	}

	public void setUser_id(User user_id) {
		this.user_id = user_id;
	}

	@Column(name="order_date")
	public String getOrder_date() {
		return order_date;
	}

	public void setOrder_date(String order_date) {
		this.order_date = order_date;
	}

	@Column(name="paid_date")
	public String getPaid_date() {
		return paid_date;
	}

	public void setPaid_date(String paid_date) {
		this.paid_date = paid_date;
	}

	@Column(name="delivery")
	public String getDelivery() {
		return delivery;
	}

	public void setDelivery(String delivery) {
		this.delivery = delivery;
	}

	@OneToOne
	@JoinColumn(name="payment_method", referencedColumnName="name")
	public Payment_Method getPayment_method() {
		return payment_method;
	}

	public void setPayment_method(Payment_Method payment_method) {
		this.payment_method = payment_method;
	}

	@Column(name="status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
