/**
 * 
 */
package com.tatalo.entities;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Michael_IT
 *
 */
@Entity
@Table(name = "billl_detail", catalog = "tatalo")
public class Bill_Detail {
	private int quantity;
	private Bill_DetailId id;

	public Bill_Detail(int quantity, Bill_DetailId id) {
		super();
		this.quantity = quantity;
		this.id = id;
	}

	public Bill_Detail() {
		super();
	}

	@Column(name="quantity")
	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	@EmbeddedId
	public Bill_DetailId getId() {
		return id;
	}

	public void setId(Bill_DetailId id) {
		this.id = id;
	}
}
