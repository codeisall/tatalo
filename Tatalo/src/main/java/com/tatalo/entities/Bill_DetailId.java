/**
 * 
 */
package com.tatalo.entities;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * @author Michael_IT
 *
 */
@Embeddable
public class Bill_DetailId implements Serializable {
	private int bill_id;
	private int product_id;

	public Bill_DetailId(int bill_id, int product_id) {
		super();
		this.bill_id = bill_id;
		this.product_id = product_id;
	}

	public Bill_DetailId() {
		super();
	}

	@Column(name = "bill_id")
	public int getBill_id() {
		return bill_id;
	}

	public void setBill_id(int bill_id) {
		this.bill_id = bill_id;
	}

	@Column(name = "product_id")
	public int getProduct_id() {
		return product_id;
	}

	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof Bill_DetailId))
			return false;
		Bill_DetailId that = (Bill_DetailId) o;
		return Objects.equals(getBill_id(), that.getBill_id()) && Objects.equals(getProduct_id(), that.getProduct_id());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return Objects.hash(getBill_id(), getProduct_id());
	}
}
