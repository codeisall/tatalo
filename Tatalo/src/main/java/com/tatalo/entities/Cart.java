/**
 * 
 */
package com.tatalo.entities;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Michael_IT
 *
 */
@Entity
@Table(name = "cart", catalog = "tatalo")
public class Cart {
	private int quantity;
	private CartId id;

	public Cart(int quantity, CartId id) {
		super();
		this.quantity = quantity;
		this.id = id;
	}

	public Cart() {
		super();
	}

	@Column(name="quantity")
	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	@EmbeddedId
	public CartId getId() {
		return id;
	}

	public void setId(CartId id) {
		this.id = id;
	}

}
