/**
 * 
 */
package com.tatalo.entities;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * @author Michael_IT
 *
 */
@Embeddable
public class CartId implements Serializable {
	private int user_id;
	private int product_id;

	public CartId(int user_id, int product_id) {
		super();
		this.user_id = user_id;
		this.product_id = product_id;
	}

	public CartId() {
		super();
	}

	@Column(name="user_id")
	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	@Column(name="product_id")
	public int getProduct_id() {
		return product_id;
	}

	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
        if (!(o instanceof CartId)) return false;
        CartId that = (CartId) o;
        return Objects.equals(getUser_id(), that.getUser_id()) &&
                Objects.equals(getProduct_id(), that.getProduct_id());
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		 return Objects.hash(getUser_id(), getProduct_id());
	}
}
