/**
 * 
 */
package com.tatalo.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * @author Michael_IT
 *
 */
@Entity
@Table(name = "paymentmethod", catalog = "tatalo")
public class Payment_Method {
	private int id;
	private String name;
	private Bill bill;

	public Payment_Method(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public Payment_Method() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@OneToOne(mappedBy="payment_method")
	public Bill getBill() {
		return bill;
	}

	public void setBill(Bill bill) {
		this.bill = bill;
	}
}
