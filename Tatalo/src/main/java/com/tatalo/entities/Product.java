/**
 * 
 */
package com.tatalo.entities;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author Michael_IT
 *
 */
@Entity
@Table(name = "product", catalog = "tatalo")
public class Product {
	private int id;
	private String name;
	private int price;
	private String description;
	private int bought;
	private int star;
	private Category category;
	private String specific_info;
	private String discount_code;
	private String images;
	private Set<Review> reviews;
	private int score;

	public Product(int id, String name, int price, String description, int bought, int star, Category category,
			String specific_info, String discount_code, String images) {
		super();
		this.id = id;
		this.name = name;
		this.price = price;
		this.description = description;
		this.bought = bought;
		this.star = star;
		this.category = category;
		this.specific_info = specific_info;
		this.discount_code = discount_code;
		this.images = images;
	}

	public Product() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "price")
	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	@Column(name = "description", columnDefinition = "TEXT")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "bought")
	public int getBought() {
		return bought;
	}

	public void setBought(int bought) {
		this.bought = bought;
	}

	@Column(name = "star")
	public int getStar() {
		return star;
	}

	public void setStar(int star) {
		this.star = star;
	}

	@ManyToOne
	@JoinColumn(name = "category_id", referencedColumnName = "id")
	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	@Column(name = "specific_info", columnDefinition = "TEXT")
	public String getSpecific_info() {
		return specific_info;
	}

	public void setSpecific_info(String specific_info) {
		this.specific_info = specific_info;
	}

	@Column(name = "discount_code")
	public String getDiscount_code() {
		return discount_code;
	}

	public void setDiscount_code(String discount_code) {
		this.discount_code = discount_code;
	}

	@Column(name = "images")
	public String getImages() {
		return images;
	}

	public void setImages(String images) {
		this.images = images;
	}

	@OneToMany(mappedBy = "product_id")
	public Set<Review> getReviews() {
		return reviews;
	}

	public void setReviews(Set<Review> reviews) {
		this.reviews = reviews;
	}

	@Column(name="score")
	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}
}
