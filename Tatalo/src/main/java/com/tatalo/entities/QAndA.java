/**
 * 
 */
package com.tatalo.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Michael_IT
 *
 */
@Entity
@Table(name = "qanda", catalog = "tatalo")
public class QAndA {
	private int id;
	private String question;
	private String answer;
	private int like_time;
	private String email;

	public QAndA(int id, String question, String answer, int like_time, String email) {
		super();
		this.id = id;
		this.question = question;
		this.answer = answer;
		this.like_time = like_time;
		this.email = email;
	}

	public QAndA() {
		super();
	}

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name="question")
	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	@Column(name="answer")
	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	@Column(name="like_time")
	public int getLike_Time() {
		return like_time;
	}

	public void setLike_Time(int like_time) {
		this.like_time = like_time;
	}

	@Column(name="email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
