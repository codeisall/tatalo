/**
 * 
 */
package com.tatalo.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Michael_IT
 *
 */
@Entity
@Table(name = "review", catalog = "tatalo")
public class Review {
	private int id;
	private User user_id;
	private Product product_id;
	private String review;
	private int star;
	private String images;

	public Review(int id, User user_id, Product product_id, String review, int star, String images) {
		super();
		this.id = id;
		this.user_id = user_id;
		this.product_id = product_id;
		this.review = review;
		this.star = star;
		this.images = images;
	}

	public Review() {
		super();
	}

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name="user_id", referencedColumnName="id")
	public User getUser_id() {
		return user_id;
	}

	public void setUser_id(User user_id) {
		this.user_id = user_id;
	}

	@ManyToOne
	@JoinColumn(name="product_id", referencedColumnName="id")
	public Product getProduct_id() {
		return product_id;
	}

	public void setProduct_id(Product product_id) {
		this.product_id = product_id;
	}

	@Column(name="review")
	public String getReview() {
		return review;
	}

	public void setReview(String review) {
		this.review = review;
	}

	@Column(name="star")
	public int getStar() {
		return star;
	}

	public void setStar(int star) {
		this.star = star;
	}

	@Column(name="iamges")
	public String getImages() {
		return images;
	}

	public void setImages(String images) {
		this.images = images;
	}
}
