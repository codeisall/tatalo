/**
 * 
 */
package com.tatalo.entities;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Michael_IT
 *
 */
@Entity
@Table(name = "user_address", catalog = "tatalo")
public class User_Address {
	@EmbeddedId
	private User_AddressId id;

	public User_Address(User_AddressId id) {
		super();
		this.id = id;
	}

	public User_Address() {
		super();
	}

	public User_AddressId getId() {
		return id;
	}

	public void setId(User_AddressId id) {
		this.id = id;
	}
}
