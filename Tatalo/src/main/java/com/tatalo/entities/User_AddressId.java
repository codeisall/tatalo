/**
 * 
 */
package com.tatalo.entities;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * @author Michael_IT
 *
 */
@Embeddable
public class User_AddressId implements Serializable {
	private int address_id;
	private int user_id;

	public User_AddressId(int address_id, int user_id) {
		super();
		this.address_id = address_id;
		this.user_id = user_id;
	}

	public User_AddressId() {
		super();
	}

	@Column(name="address_id")
	public int getAddress_id() {
		return address_id;
	}

	public void setAddress_id(int address_id) {
		this.address_id = address_id;
	}

	@Column(name="user_id")
	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
        if (!(o instanceof User_AddressId)) return false;
        User_AddressId that = (User_AddressId) o;
        return Objects.equals(getAddress_id(), that.getAddress_id()) &&
                Objects.equals(getUser_id(), that.getUser_id());
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return Objects.hash(getAddress_id(), getUser_id());
	}
}
