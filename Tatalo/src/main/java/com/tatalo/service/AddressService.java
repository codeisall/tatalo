/**
 * 
 */
package com.tatalo.service;

import java.util.ArrayList;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tatalo.dao.Dao;
import com.tatalo.entities.Address;

/**
 * @author Michael_IT
 *
 */
@Transactional	
@Service	
public class AddressService {
	@Autowired
	Dao<Address> AddressDao;
	
	@Autowired
	SessionFactory sessionFactory;
	
	public ArrayList<Address> getAll (){
		return AddressDao.getAll();
	}
	public Address getById(Address t) {
		return AddressDao.getById(t);
	}
	
	public Address add(Address t) {
		return AddressDao.add(t);
	}
	
	public boolean update(Address t) {
		return AddressDao.update(t);
	}
	
	public boolean delete(Address t) {
		return AddressDao.delete(t);
	}
	
	public boolean deleteById(Address t) {
		return AddressDao.deleteById(t);
	}
}
