/**
 * 
 */
package com.tatalo.service;

import java.util.ArrayList;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tatalo.dao.Dao;
import com.tatalo.entities.Bill;

/**
 * @author Michael_IT
 *
 */
@Transactional	
@Service
public class BillService {
	@Autowired
	Dao<Bill> BillDao;
	
	@Autowired
	SessionFactory sessionFactory;
	
	public ArrayList<Bill> getAll (){
		return BillDao.getAll();
	}
	public Bill getById(Bill t) {
		return BillDao.getById(t);
	}
	
	public Bill add(Bill t) {
		return BillDao.add(t);
	}
	
	public boolean update(Bill t) {
		return BillDao.update(t);
	}
	
	public boolean delete(Bill t) {
		return BillDao.delete(t);
	}
	
	public boolean deleteById(Bill t) {
		return BillDao.deleteById(t);
	}
}
