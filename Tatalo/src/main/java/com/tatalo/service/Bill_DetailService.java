/**
 * 
 */
package com.tatalo.service;

import java.util.ArrayList;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tatalo.dao.Dao;
import com.tatalo.entities.Bill_Detail;

/**
 * @author Michael_IT
 *
 */
@Transactional
@Service
public class Bill_DetailService {
	@Autowired
	Dao<Bill_Detail> Bill_DetailDao;
	
	@Autowired
	SessionFactory sessionFactory;
	
	public ArrayList<Bill_Detail> getAll (){
		return Bill_DetailDao.getAll();
	}
	public Bill_Detail getById(Bill_Detail t) {
		return Bill_DetailDao.getById(t);
	}
	
	public Bill_Detail add(Bill_Detail t) {
		return Bill_DetailDao.add(t);
	}
	
	public boolean update(Bill_Detail t) {
		return Bill_DetailDao.update(t);
	}
	
	public boolean delete(Bill_Detail t) {
		return Bill_DetailDao.delete(t);
	}
	
	public boolean deleteById(Bill_Detail t) {
		return Bill_DetailDao.deleteById(t);
	}
}	
