/**
 * 
 */
package com.tatalo.service;

import java.util.ArrayList;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tatalo.dao.Dao;
import com.tatalo.entities.Brand;

/**
 * @author Michael_IT
 *
 */
@Transactional
@Service
public class BrandService {
	@Autowired
	Dao<Brand> BrandDao;
	
	@Autowired
	SessionFactory sessionFactory;
	
	public ArrayList<Brand> getAll (){
		return BrandDao.getAll();
	}
	public Brand getById(Brand t) {
		return BrandDao.getById(t);
	}
	
	public Brand add(Brand t) {
		return BrandDao.add(t);
	}
	
	public boolean update(Brand t) {
		return BrandDao.update(t);
	}
	
	public boolean delete(Brand t) {
		return BrandDao.delete(t);
	}
	
	public boolean deleteById(Brand t) {
		return BrandDao.deleteById(t);
	}
}
