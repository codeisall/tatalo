/**
 * 
 */
package com.tatalo.service;

import java.util.ArrayList;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tatalo.dao.Dao;
import com.tatalo.entities.Cart;

/**
 * @author Michael_IT
 *
 */
@Transactional
@Service
public class CartService {
	@Autowired
	Dao<Cart> CartDao;
	
	@Autowired
	SessionFactory sessionFactory;
	
	public ArrayList<Cart> getAll (){
		return CartDao.getAll();
	}
	public Cart getById(Cart t) {
		return CartDao.getById(t);
	}
	
	public Cart add(Cart t) {
		return CartDao.add(t);
	}
	
	public boolean update(Cart t) {
		return CartDao.update(t);
	}
	
	public boolean delete(Cart t) {
		return CartDao.delete(t);
	}
	
	public boolean deleteById(Cart t) {
		return CartDao.deleteById(t);
	}
}
