/**
 * 
 */
package com.tatalo.service;

import java.util.ArrayList;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tatalo.dao.Dao;
import com.tatalo.entities.Category;

/**
 * @author Michael_IT
 *
 */
@Transactional
@Service
public class CategoryService {
	@Autowired
	Dao<Category> CategoryDao;
	
	@Autowired
	SessionFactory sessionFactory;
	
	public ArrayList<Category> getAll (){
		return CategoryDao.getAll();
	}
	public Category getById(Category t) {
		return CategoryDao.getById(t);
	}
	
	public Category add(Category t) {
		return CategoryDao.add(t);
	}
	
	public boolean update(Category t) {
		return CategoryDao.update(t);
	}
	
	public boolean delete(Category t) {
		return CategoryDao.delete(t);
	}
	
	public boolean deleteById(Category t) {
		return CategoryDao.deleteById(t);
	}
}
