/**
 * 
 */
package com.tatalo.service;

import java.util.ArrayList;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tatalo.dao.Dao;
import com.tatalo.entities.City;

/**
 * @author Michael_IT
 *
 */
@Transactional
@Service
public class CityService {
	@Autowired
	Dao<City> CityDao;
	
	@Autowired
	SessionFactory sessionFactory;
	
	public ArrayList<City> getAll (){
		return CityDao.getAll();
	}
	public City getById(City t) {
		return CityDao.getById(t);
	}
	
	public City add(City t) {
		return CityDao.add(t);
	}
	
	public boolean update(City t) {
		return CityDao.update(t);
	}
	
	public boolean delete(City t) {
		return CityDao.delete(t);
	}
	
	public boolean deleteById(City t) {
		return CityDao.deleteById(t);
	}
}
