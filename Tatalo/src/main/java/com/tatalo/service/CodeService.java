/**
 * 
 */
package com.tatalo.service;

import java.util.ArrayList;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tatalo.dao.Dao;
import com.tatalo.entities.Code;

/**
 * @author Michael_IT
 *
 */
@Transactional
@Service
public class CodeService {
	@Autowired
	Dao<Code> CodeDao;
	
	@Autowired
	SessionFactory sessionFactory;
	
	public ArrayList<Code> getAll (){
		return CodeDao.getAll();
	}
	public Code getById(Code t) {
		return CodeDao.getById(t);
	}
	
	public Code add(Code t) {
		return CodeDao.add(t);
	}
	
	public boolean update(Code t) {
		return CodeDao.update(t);
	}
	
	public boolean delete(Code t) {
		return CodeDao.delete(t);
	}
	
	public boolean deleteById(Code t) {
		return CodeDao.deleteById(t);
	}
}
