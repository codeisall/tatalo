/**
 * 
 */
package com.tatalo.service;

import java.util.ArrayList;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tatalo.dao.Dao;
import com.tatalo.entities.District;

/**
 * @author Michael_IT
 *
 */
@Transactional
@Service
public class DistrictService {
	@Autowired
	Dao<District> DistrictDao;
	
	@Autowired
	SessionFactory sessionFactory;
	
	public ArrayList<District> getAll (){
		return DistrictDao.getAll();
	}
	public District getById(District t) {
		return DistrictDao.getById(t);
	}
	
	public District add(District t) {
		return DistrictDao.add(t);
	}
	
	public boolean update(District t) {
		return DistrictDao.update(t);
	}
	
	public boolean delete(District t) {
		return DistrictDao.delete(t);
	}
	
	public boolean deleteById(District t) {
		return DistrictDao.deleteById(t);
	}
}
