/**
 * 
 */
package com.tatalo.service;

import java.util.ArrayList;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tatalo.dao.Dao;
import com.tatalo.entities.Payment_Method;

/**
 * @author Michael_IT
 *
 */
@Transactional
@Service
public class Payment_MethodService {
	@Autowired
	Dao<Payment_Method> Payment_MethodDao;
	
	@Autowired
	SessionFactory sessionFactory;
	
	public ArrayList<Payment_Method> getAll (){
		return Payment_MethodDao.getAll();
	}
	public Payment_Method getById(Payment_Method t) {
		return Payment_MethodDao.getById(t);
	}
	
	public Payment_Method add(Payment_Method t) {
		return Payment_MethodDao.add(t);
	}
	
	public boolean update(Payment_Method t) {
		return Payment_MethodDao.update(t);
	}
	
	public boolean delete(Payment_Method t) {
		return Payment_MethodDao.delete(t);
	}
	
	public boolean deleteById(Payment_Method t) {
		return Payment_MethodDao.deleteById(t);
	}
}
