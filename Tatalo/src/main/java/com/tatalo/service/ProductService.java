/**
 * 
 */
package com.tatalo.service;

import java.util.ArrayList;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tatalo.dao.Dao;
import com.tatalo.entities.Product;

/**
 * @author Michael_IT
 *
 */
@Transactional
@Service
public class ProductService {
	@Autowired
	Dao<Product> ProductDao;
	
	@Autowired
	SessionFactory sessionFactory;
	
	public ArrayList<Product> getAll (){
		return ProductDao.getAll();
	}
	public Product getById(Product t) {
		return ProductDao.getById(t);
	}
	
	public Product add(Product t) {
		return ProductDao.add(t);
	}
	
	public boolean update(Product t) {
		return ProductDao.update(t);
	}
	
	public boolean delete(Product t) {
		return ProductDao.delete(t);
	}
	
	public boolean deleteById(Product t) {
		return ProductDao.deleteById(t);
	}
}
