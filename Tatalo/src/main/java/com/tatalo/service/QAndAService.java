/**
 * 
 */
package com.tatalo.service;

import java.util.ArrayList;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tatalo.dao.Dao;
import com.tatalo.entities.QAndA;

/**
 * @author Michael_IT
 *
 */
@Transactional
@Service
public class QAndAService {
	@Autowired
	Dao<QAndA> QAndADao;
	
	@Autowired
	SessionFactory sessionFactory;
	
	public ArrayList<QAndA> getAll (){
		return QAndADao.getAll();
	}
	public QAndA getById(QAndA t) {
		return QAndADao.getById(t);
	}
	
	public QAndA add(QAndA t) {
		return QAndADao.add(t);
	}
	
	public boolean update(QAndA t) {
		return QAndADao.update(t);
	}
	
	public boolean delete(QAndA t) {
		return QAndADao.delete(t);
	}
	
	public boolean deleteById(QAndA t) {
		return QAndADao.deleteById(t);
	}
}
