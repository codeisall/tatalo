/**
 * 
 */
package com.tatalo.service;

import java.util.ArrayList;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tatalo.dao.Dao;
import com.tatalo.entities.Review;

/**
 * @author Michael_IT
 *
 */
@Transactional
@Service
public class ReviewService {
	@Autowired
	Dao<Review> ReviewDao;
	
	@Autowired
	SessionFactory sessionFactory;
	
	public ArrayList<Review> getAll (){
		return ReviewDao.getAll();
	}
	public Review getById(Review t) {
		return ReviewDao.getById(t);
	}
	
	public Review add(Review t) {
		return ReviewDao.add(t);
	}
	
	public boolean update(Review t) {
		return ReviewDao.update(t);
	}
	
	public boolean delete(Review t) {
		return ReviewDao.delete(t);
	}
	
	public boolean deleteById(Review t) {
		return ReviewDao.deleteById(t);
	}
}
