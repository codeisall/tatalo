/**
 * 
 */
package com.tatalo.service;

import java.util.ArrayList;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tatalo.dao.Dao;
import com.tatalo.entities.Address;
import com.tatalo.entities.User;

/**
 * @author Michael_IT
 *
 */
@Transactional
@Service
public class UserService {
	@Autowired
	Dao<User> UserDao;
	
	@Autowired
	SessionFactory sessionFactory;
	
	public ArrayList<User> getAll (){
		return UserDao.getAll();
	}
	public User getById(User t) {
		return UserDao.getById(t);
	}
	
	public User add(User t) {
		return UserDao.add(t);
	}
	
	public boolean update(User t) {
		return UserDao.update(t);
	}
	
	public boolean delete(User t) {
		return UserDao.delete(t);
	}
	
	public boolean deleteById(User t) {
		return UserDao.deleteById(t);
	}
}
